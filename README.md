# LabVIEW Quick Drop VI Analyzer for Git Users

This is the source code for the Quick Drop Plugin for quickly running VI Analyzer Tests based on a configuration files.

## Installation

1. Use the VI Package Manager [VIPM](https://vipm.io) to install the package.
2. Or download source code and install to your Quick Drop Plugin Folder.

## Usage

```mermaid
graph LR;
id1[Ctr + Space]-->id2[Ctrl+V]-->id3[First Configuration File];
id1-->id4[Ctr+Shift+V]-->id5[Second Configuration File];
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Dependencies

1. VI Analyzer Toolkit
2. Git SCM for Windows

## License
[BSD3](https://choosealicense.com/licenses/bsd-3-clause/)

# Author

Felipe Pinheiro Silva

## Contributors
- Carlos Henrique Szollosi
- Felipe Lorenzetti
